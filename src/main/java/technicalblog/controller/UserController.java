package technicalblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import technicalblog.model.Post;
import technicalblog.model.User;
import technicalblog.service.PostService;
import technicalblog.service.UserService;

import java.util.ArrayList;


//Tells this class is a controller.
@Controller
public class UserController {

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;
    //Controllers with specific name mapped to user/login file.
    @RequestMapping("users/login")
    public String login(){
        return "users/login";
    }
    //Controllers with specific name mapped to user/registration file.
    @RequestMapping("users/registration")
    public String registration(){
        return "users/registration";
    }


    @RequestMapping(value = "users/logout",method = RequestMethod.POST)
    public String logout(User user,Model model) {
        ArrayList<Post> allPosts = postService.getAllPosts();
        model.addAttribute("posts",allPosts);
        return "index";
    }


    //After successful registration it goes to users/login to again login
    @RequestMapping(value = "users/registration",method = RequestMethod.POST)
    public String registration(User user) {
        return "users/login";
    }

    //When 2 Mappings have same name, we have to give value to the second one.
    //here, we used RequestMethod.Post because we are getting sensitive info.
    @RequestMapping(value = "users/login",method = RequestMethod.POST)
    public String loginUser(User user){
        if(userService.login(user))
            return "redirect:/posts";
        return "users/login";
        //we are redirecting the user/login page to posts.html
        // with the "user" model object having data of User in it.
        //As we have passed during submission of form in login.html
    }
}
