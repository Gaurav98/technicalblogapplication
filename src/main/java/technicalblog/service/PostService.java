package technicalblog.service;

import org.springframework.stereotype.Service;
import technicalblog.model.Post;

import java.util.ArrayList;
import java.util.Date;


// Service is basically performing CRUD operations and adding data to model
@Service
public class PostService {

    public ArrayList<Post> getAllPosts(){
        ArrayList<Post> posts =new ArrayList<>();

        Post post1 =new Post();
        post1.setTitle("Post 1");
        post1.setBody("Body 1");
        post1.setDate(new Date());

        Post post2 =new Post();
        post2.setTitle("Post 2");
        post2.setBody("Body 2");
        post2.setDate(new Date());

        posts.add(post1);
        posts.add(post2);
        return posts;
    }

    public ArrayList<Post> getOnePost(){
        ArrayList<Post> posts =new ArrayList<>();

        Post post1 =new Post();
        post1.setTitle("This is your First Post as User1");
        post1.setBody("User Body");
        post1.setDate(new Date());

        posts.add(post1);
        return posts;
    }


    public void createPost(Post newPost){

    }

}
